import React, { useState } from 'react';
import { SPAM_WORLDS } from './SPAM';

function EmailSpamChecker() {
  const [email, setEmail] = useState('');
  const [result, setResult] = useState(null);
  const [spamWord, setSpamWord] = useState(null);

  function containsSpamWords(email) {
    setResult(null);
    setSpamWord(null);
    const lowercaseEmail = email.toLowerCase();
      for (const spamWord of SPAM_WORLDS) {
      if (lowercaseEmail.includes(spamWord.toLowerCase())) {
        setSpamWord(spamWord)
        return true; 
      }
    }  
    return false;
  }

  function checkForSpam() {
    if (containsSpamWords(email)) {
      setResult(true);
    } else {
      setResult(false);
    }
  }

  return (
    <div className="container">
      <h2>KYO Email Spam Checker</h2>
      <div style={{display:'flex',flexDirection:'column'}}>

      <label htmlFor="email">Enter your text :</label>
      <textarea
        // type="text"
        rows={10}
        id="email"
        placeholder="Email Text"
        style={{margin:'1rem 0'}}
        value={email}
        onChange={(e) => setEmail(e.target.value)}
        />
        </div>
      <button onClick={checkForSpam}>Check for Spam</button>
      {result!=null && <p id="result" className={result?'spam':'not_spam'}>{result?"This email contains spam words.":'This email is clean.'}</p>}
      {spamWord && <p style={{color:'red'}}>spam word in the email :{spamWord}</p>}
    </div>
  );
}

export default EmailSpamChecker
